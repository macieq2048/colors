# Colors
<hr>
*todo* come up with some creative project name
<hr>
It's basically gonna be HUE like game
On the board there gonna be tiles colored accordingly to gradient, then the
game gonna mix them and the goal is to put them back in place to create initial
gradient.
<br/>
<img src="https://media.giphy.com/media/gaaDF2kXmlU7kfC59P/giphy.gif" width="400" height="400" />
<br/>
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
